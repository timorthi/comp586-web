# Timothy Ng's COMP586 Client
This is an Angular 6 project. It serves as the front-end API for the course project in Prof. Rabinovich's COMP586 class for Fall 2018.

A deployed version of this app is available at https://homeboard-web.netlify.com.

---

## Installation
Using Docker and Docker Compose, you can get the project up and running via
```bash
$ docker-compose up
```
This will provision the required backing services (the Ruby on Rails API, and the database) as well as the client.

Otherwise, install the following:
* Everything in the requirements for the [back-end API](https://gitlab.com/timorthi/comp586-api).
* NPM 6.4+
* Angular 6.2.3

Start up the backing services, then start the development server for this client:
```bash
$ npm start
```

The app can then be accessed with root `localhost:4200`.

---

## Source Code
The main source code can be found in `src/app/`. Within this directory:
* The directories prefixed with `_` are shared entities.
* At the root level there are the files for the top-level Angular module, `app.module`.
* The other two Angular modules are `auth` and `todo` in their respective directories.

---

## Tests
No tests were written for the client side.
