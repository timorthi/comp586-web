export const errors = {
    401: {
        name: 'Unauthorized',
        message: "You need to be logged in to access this resource.",
    },
    403: {
        name: 'Forbidden',
        message: "You don't have the correct permissions to access this resource.",
    },
    404: {
        name: 'Not Found',
        message: "The resource you're trying to access doesn't exist.",
    },
    500: {
        name: 'Internal Server Error',
        message: 'Something went wrong internally. Sorry!',
    }
};
