import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { routing } from './auth.routing';

import { UserService, AuthenticationService } from '../_services';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    routing
  ],
  declarations: [
    SignupComponent,
    LoginComponent,
  ],
  exports: [
    SignupComponent,
    LoginComponent
  ],
  providers: [
    UserService,
    AuthenticationService
  ]
})
export class AuthModule { }
