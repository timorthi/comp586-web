import { Component, OnInit, Input } from '@angular/core';

import { TodoItem } from '../../_models';

@Component({
  selector: 'app-show-items',
  templateUrl: './show-items.component.html',
  styleUrls: ['./show-items.component.scss']
})
export class ShowItemsComponent implements OnInit {
  @Input() todoItems: TodoItem[];

  constructor() { }

  ngOnInit() {
  }
}
