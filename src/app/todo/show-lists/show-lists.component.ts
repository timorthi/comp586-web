import { Component, OnInit, Input } from '@angular/core';

import { TodoList } from '../../_models/todolist';

@Component({
  selector: 'app-show-lists',
  templateUrl: './show-lists.component.html',
  styleUrls: ['./show-lists.component.scss']
})
export class ShowListsComponent implements OnInit {
  @Input() todoLists: TodoList[];

  constructor() { }

  ngOnInit() {
  }

}
