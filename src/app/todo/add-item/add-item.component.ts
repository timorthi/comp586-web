import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { TodoItem } from '../../_models';
import { TodoService } from '../../_services';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.scss']
})
export class AddItemComponent implements OnInit {
  @Output() addItemEvent = new EventEmitter<TodoItem>();
  @Input() todoListId: Number;
  todoItemForm: FormGroup;
  submitted: Boolean;

  constructor(
    private formBuilder: FormBuilder,
    private todoService: TodoService,
  ) { }

  ngOnInit() {
    this.submitted = false;
    this.todoItemForm = this.formBuilder.group({
      description: ['', Validators.required],
    });
  }

  onSubmit() {
    this.submitted = true;

    if (this.todoItemForm.invalid) {
      return;
    }

    this.todoService
      .addTodoItem(this.todoListId, this.todoItemForm.value)
      .subscribe(resp => {
        // Propagate results to ListDetailComponent
        this.addItemEvent.emit(resp['todo_item']);
        this.todoItemForm.reset();
        this.submitted = false;
      });
  }

  // convenience getter for easy access to form fields in template
  get f() { return this.todoItemForm.controls; }
}
