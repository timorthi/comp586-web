import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { TodoService, AuthenticationService } from '../../_services';
import { TodoList } from '../../_models/todolist';

@Component({
  selector: 'app-add-list',
  templateUrl: './add-list.component.html',
  styleUrls: ['./add-list.component.scss']
})
export class AddListComponent implements OnInit {
  @Output() addListEvent = new EventEmitter<TodoList>();
  todoListForm: FormGroup;
  submitted: Boolean;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthenticationService,
    private todoService: TodoService,
  ) { }

  ngOnInit() {
    this.submitted = false;
    this.todoListForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: [''],
    });
  }

  onSubmit() {
    this.submitted = true;

    if (this.todoListForm.invalid) {
      return;
    }

    const userId: Number = this.authService.getCurrentUser().id;
    this.todoService.createTodoList(userId, this.todoListForm.value)
      .subscribe(resp => {
        // Propagate results to TodoComponent
        this.addListEvent.emit(resp['todo_list']);
        this.todoListForm.reset();
        this.submitted = false;
      });
  }


  // convenience getter for easy access to form fields in template
  get f() { return this.todoListForm.controls; }
}
