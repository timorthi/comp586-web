import { Component, OnInit } from '@angular/core';
import { TodoList } from '../../_models/todolist';
import { User } from '../../_models';
import { TodoService, AuthenticationService } from '../../_services';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {
  todoLists: TodoList[] = [];
  nameForDisplay: String;

  constructor(
    private todoService: TodoService,
    private authService: AuthenticationService
  ) { }

  ngOnInit() {
    const user: User = this.authService.getCurrentUser();
    this.setDisplayName(user);
    this.fetchAndSetTodoLists(user.id);
  }

  private setDisplayName(user: User) {
    this.nameForDisplay = user.preferred_name || user.first_name;
  }

  private fetchAndSetTodoLists(userId: Number) {
    this.todoService
      .getTodoLists(userId)
      .subscribe(resp => {
        this.todoLists = resp['todo_lists'];
      });
  }

  /*
  * Callback function for when the add-list component's addListEvent fires.
  */
  addList(todoList: TodoList) {
    this.todoLists.push(todoList);
  }
}
