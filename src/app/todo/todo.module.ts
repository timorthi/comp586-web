import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TodoComponent } from './todo/todo.component';
import { ShowListsComponent } from './show-lists/show-lists.component';
import { AddListComponent } from './add-list/add-list.component';
import { ListDetailComponent } from './list-detail/list-detail.component';
import { ShowItemsComponent } from './show-items/show-items.component';
import { AddItemComponent } from './add-item/add-item.component';
import { routing } from './todo.routing';

import { TodoService } from '../_services';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    routing,
  ],
  declarations: [
    TodoComponent,
    ShowListsComponent,
    AddListComponent,
    ListDetailComponent,
    ShowItemsComponent,
    AddItemComponent,
  ],
  exports: [TodoComponent],
  providers: [TodoService],
})
export class TodoModule { }
