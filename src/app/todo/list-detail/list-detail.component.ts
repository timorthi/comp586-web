import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { TodoService } from '../../_services';
import { TodoList, TodoItem } from '../../_models';

@Component({
  selector: 'app-list-detail',
  templateUrl: './list-detail.component.html',
  styleUrls: ['./list-detail.component.scss']
})
export class ListDetailComponent implements OnInit {
  todoList: TodoList;
  todoItems: TodoItem[];

  constructor(
    private todoService: TodoService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      const todoListId: Number = parseInt(params.get('id'), 10);
      this.fetchAndSetTodoList(todoListId);
    });
  }

  private fetchAndSetTodoList(todoListId: Number) {
    this.todoService
      .getTodoList(todoListId)
      .subscribe(resp => {
        this.todoList = resp['todo_list'];
        this.todoItems = resp['todo_list']['todo_items'];
      });
  }

  /*
  * Callback function for when the add-item component's addItemEvent fires.
  */
  addItem(todoItem: TodoItem) {
    this.todoItems.push(todoItem);
  }
}
