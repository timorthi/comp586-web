import { Routes, RouterModule } from '@angular/router';

import { TodoComponent } from './todo/todo.component';
import { ListDetailComponent } from './list-detail/list-detail.component';

import { AuthGuard } from '../_guards';

const todoRoutes: Routes = [
    { path: 'dashboard', component: TodoComponent, canActivate: [AuthGuard] },
    { path: 'lists/:id', component: ListDetailComponent },
];

export const routing = RouterModule.forChild(todoRoutes);
