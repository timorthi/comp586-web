import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { errors } from '../../config';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit {
  errorCode: number;
  errorCodeName: string;
  errorMessage: string;

  constructor(
    private router: Router,
  ) { }

  ngOnInit() {
    this.errorCode = parseInt(this.router.url.substring(1), 10);
    this.errorCodeName = errors[this.errorCode].name;
    this.errorMessage = errors[this.errorCode].message;
  }
}
