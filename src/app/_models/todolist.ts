export class TodoList {
    id: number;
    title: string;
    description: string;
}
