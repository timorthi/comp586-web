enum Status {
    DONE = 'done',
    PENDING = 'pending'
}

export class TodoItem {
    id: number;
    description: string;
    status: Status;
}
