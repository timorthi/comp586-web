export class User {
    id: number;
    email: string;
    password: string;
    first_name: string;
    last_name: string;
    preferred_name: string;
    activated_at: Date;
}
