import { Routes, RouterModule } from '@angular/router';

import { ErrorComponent } from './_components';

const appRoutes: Routes = [
    { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
    { path: '401', component: ErrorComponent },
    { path: '403', component: ErrorComponent },
    { path: '404', component: ErrorComponent },
    { path: '500', component: ErrorComponent },
    { path: '**', redirectTo: '/404' }
];

export const routing = RouterModule.forRoot(appRoutes);
