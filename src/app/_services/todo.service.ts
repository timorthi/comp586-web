import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { TodoList, TodoItem } from '../_models';


@Injectable()
export class TodoService {

    constructor(
        private http: HttpClient,
    ) { }

    getTodoLists(userId: Number) {
        return this.http.get(`${environment.apiUrl}/users/${userId}/todo_lists`);
    }

    createTodoList(userId: Number, todoList: TodoList) {
        return this.http.post(`${environment.apiUrl}/users/${userId}/todo_lists`, { todo_list: todoList });
    }

    getTodoList(todoListId: Number) {
        return this.http.get(`${environment.apiUrl}/todo_lists/${todoListId}`);
    }

    addTodoItem(todoListId: Number, todoItem: TodoItem) {
        return this.http.post(`${environment.apiUrl}/todo_lists/${todoListId}/todo_items`, { todo_item: todoItem });
    }
}
