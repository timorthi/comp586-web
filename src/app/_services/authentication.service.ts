import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { User } from '../_models';


@Injectable()
export class AuthenticationService {
  constructor(
    private http: HttpClient
  ) { }

  login(email: string, password: string) {
    return this.http.post<any>(`${environment.apiUrl}/sessions`, { user: { email: email, password: password } });
  }

  setAuth(user: object, jwt: string) {
    localStorage.setItem('currentUser', JSON.stringify(user));
    localStorage.setItem('accessToken', JSON.stringify(jwt));
  }

  getCurrentUser(): User {
    return this.isLoggedIn() ? JSON.parse(localStorage.getItem('currentUser')) : null;
  }

  getAccessToken(): String {
    return this.isLoggedIn() ? JSON.parse(localStorage.getItem('accessToken')) : null;
  }

  isLoggedIn(): Boolean {
    return localStorage.getItem('currentUser') !== null && localStorage.getItem('accessToken') !== null;
  }

  logout() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('accessToken');
  }
}
